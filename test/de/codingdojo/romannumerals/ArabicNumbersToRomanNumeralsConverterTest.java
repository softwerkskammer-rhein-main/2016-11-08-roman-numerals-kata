package de.codingdojo.romannumerals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ArabicNumbersToRomanNumeralsConverterTest {

    @Parameterized.Parameters(name = "{index}: Arabic {0} -> {1} Roman")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, "I"},
                {1000, "M"},

                {2, "II"},
                {3, "III"},

                {4, "IV"},

                {91, "XCI"}
        });
    }

    @Parameter(0)
    public int arabicNumber;

    @Parameter(1)
    public String expectedRomanNumber;

    @org.junit.Test
    public void convertGeneric() throws Exception {
        assertEquals(expectedRomanNumber, ArabicNumbersToRomanNumeralsConverter.convert(arabicNumber));
    }
}
