package de.codingdojo.romannumerals;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ArabicNumbersToRomanNumeralsConverter {

    public static final Map<Integer, String> ARABIC_TO_ROMAN_NUMERALS;

    static {
        ARABIC_TO_ROMAN_NUMERALS = new LinkedHashMap<>();
        ARABIC_TO_ROMAN_NUMERALS.put(1000, "M");
        ARABIC_TO_ROMAN_NUMERALS.put(500, "D");
        ARABIC_TO_ROMAN_NUMERALS.put(100, "C");
        ARABIC_TO_ROMAN_NUMERALS.put(50, "L");
        ARABIC_TO_ROMAN_NUMERALS.put(10, "X");
        ARABIC_TO_ROMAN_NUMERALS.put(5, "V");
        ARABIC_TO_ROMAN_NUMERALS.put(1, "I");
    }

    public static String convert(int arabicNumber) {
        String result = "";

        for (int i : ARABIC_TO_ROMAN_NUMERALS.keySet()) {
            boolean isMultiDigit = isMultiDigit(i);

            if (isMultiDigit) {
                int counter = 0;
                while (arabicNumber >= i && counter < 3) {
                    counter++;
                    arabicNumber -= i;
                    result += ARABIC_TO_ROMAN_NUMERALS.get(i);
                }
            } else {
                if (arabicNumber >= i) {
                    arabicNumber -= i;
                    result += ARABIC_TO_ROMAN_NUMERALS.get(i);
                }
            }

        }
        return result;
    }

    private static boolean isMultiDigit(int arabicNumber) {
        switch (arabicNumber) {
            case 5:
            case 50:
            case 500:
                return false;
            default:
                return true;
        }
    }
}
